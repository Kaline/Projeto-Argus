#define THINGER_SERVER "127.0.1.1"  

#include <WiFi.h>
#include <ThingerESP32.h>

#define USERNAME "kaline"
#define DEVICE_ID "esp32"
#define DEVICE_CREDENTIAL "QrTWBa5T4FAy"

#define SSID "Rede 13"
#define SSID_PASSWORD "blackcat1"

// define your board pin here
#define LED_PIN 16

ThingerESP32 thing(USERNAME, DEVICE_ID, DEVICE_CREDENTIAL);

void setup() {
  pinMode(LED_PIN, OUTPUT);

  thing.add_wifi(SSID, SSID_PASSWORD);

  // digital pin control example (i.e. turning on/off a light, a relay, configuring a parameter, etc)
  thing["led"] << digitalPin(LED_PIN);

  // resource output example (i.e. reading a sensor value)
  thing["millis"] >> outputValue(millis());

  // more details at http://docs.thinger.io/arduino/
}

void loop() {
  thing.handle();
}
